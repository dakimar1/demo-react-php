This is a simply project using React and php and websocket 

## Backend 

To run the server go in backend and run :

### `composer install`

then run :

### `php server.php`


## Frontend 

go in frontend and run :

### `npm install`

and then 

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
